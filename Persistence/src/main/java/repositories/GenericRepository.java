package repositories;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import javax.transaction.Transaction;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

public abstract class GenericRepository<T, K> {
    protected final EntityManager entityManager;
    protected final Class<T> entityClass;

    @SuppressWarnings("unchecked")
    protected GenericRepository() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
        this.entityManager = EntityManagerUtil.get();
    }

    public void create(T entity) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(entity);
        transaction.commit();
    }

    public T read(K id) {
        return entityManager.find(entityClass, id);
    }

    public T update(T entity) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        T merge = entityManager.merge(entity);
        transaction.commit();
        return merge;
    }

    public void delete(K id) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        T read = read(id);
        entityManager.remove(read);
        transaction.commit();
    }

    public List<T> findAll() {
        List<T> allObjects = new ArrayList();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            allObjects = entityManager.createQuery("Select t from " + entityClass.getSimpleName() + " t", entityClass).getResultList();
            transaction.commit();
            return allObjects;
        } catch (Exception e) {
            transaction.rollback();
            return null;
        }
    }


}
