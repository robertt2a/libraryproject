package repositories;

import entities.Book;

import javax.persistence.TypedQuery;
import java.util.List;

public class BookRepository extends GenericRepository<Book, Integer> {
    public List<Book> findAll(){
        TypedQuery<Book> query = entityManager.createQuery("select b from Book b", entityClass);
        return query.getResultList();

    }
}
