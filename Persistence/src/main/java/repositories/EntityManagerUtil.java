package repositories;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerUtil {
   private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("library");
    private static EntityManager entityManager = null;

   static EntityManager get(){
       if (entityManager==null) {
           return entityManagerFactory.createEntityManager();
       }
       return entityManager;
    }
}
