package servlets;

import entities.Borrower;
import entities.Librarian;
import services.BorrowerService;
import services.LibrarianService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private BorrowerService borrowerService = new BorrowerService();
    private LibrarianService librarianService = new LibrarianService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("Username");
        String password = request.getParameter("Password");

        HttpSession session = request.getSession();


        List<Borrower> borrowers = borrowerService.findAllBorrowers();
        List<Librarian> librarians = librarianService.findAllLibrarians();

        Optional borrower = Optional.empty();
        Optional librarian = Optional.empty();

        if (request.getParameter("login") != null) {
            borrower = borrowers.stream()
                    .filter(b -> b.getUsername().equals(username) && b.getPassword().equals(password))
                    .findAny();
        } else if (request.getParameter("adminLogin") != null) {
            librarian = librarians.stream()
                    .filter(l -> l.getUsername().equals(username) && l.getPassword().equals(password))
                    .findAny();
        }
        session.setAttribute("admin", librarian);
        session.setAttribute("user", borrower);
        if (!borrower.isPresent() && !librarian.isPresent()) {
            response.sendRedirect("loginFailed.jsp");
        } else if (borrower.isPresent()) {
            request.getRequestDispatcher("/home").forward(request, response);
        } else {
            request.getRequestDispatcher("/home").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
