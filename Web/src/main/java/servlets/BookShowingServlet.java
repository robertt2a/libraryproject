package servlets;

import services.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/show")
public class BookShowingServlet extends HttpServlet {
    private BookService service = new BookService();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer bookId = Integer.valueOf(request.getParameter("bookId"));
        request.setAttribute("book", service.findBook(bookId));
        request.getRequestDispatcher("bookDetails.jsp").forward(request,response);
    }
}
