package servlets;

import dto.BookDTO;
import entities.Borrower;
import entities.Librarian;
import services.BookService;
import enums.Action;
import services.BorrowerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet("/home")
public class HomeServlet extends HttpServlet {

    private BookService bookService = new BookService();
    private BorrowerService borrowerService = new BorrowerService();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<BookDTO> books = bookService.findAll();
        request.setAttribute("borrowers", borrowerService.findAllBorrowers());
        request.setAttribute("books", books);
        HttpSession session = request.getSession();
        Optional<Librarian> librarian = (Optional<Librarian>) session.getAttribute("admin");
        Optional<Borrower> borrower = (Optional<Borrower>) session.getAttribute("user");
        if (!borrower.isPresent() && librarian.isPresent()) {
            request.getRequestDispatcher("home.jsp").forward(request, response);
        } else if (borrower.isPresent()) {
            request.getRequestDispatcher("userHomepage.jsp").forward(request, response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Action action = Action.valueOf(request.getParameter("action"));

        switch (action) {
            case ADD:
                response.sendRedirect("/add");
                break;
            case DELETE:
                Integer bookId = Integer.parseInt(request.getParameter("bookId"));
                request.setAttribute("bookId", bookId);
                request.getRequestDispatcher("/delete").forward(request, response);
                break;
            case SHOW:
                bookId = Integer.parseInt(request.getParameter("bookId"));
                request.setAttribute("bookId", bookId);
                request.getRequestDispatcher("/show").forward(request, response);
            case EDIT:
                bookId = Integer.parseInt(request.getParameter("bookId"));
                response.sendRedirect("/edit?bookId=" + bookId);
                break;
            case GET:
                doGet(request, response);
                break;
            case LOGOUT:
                request.getRequestDispatcher("/logout").forward(request, response);
                break;
        }
    }
}
