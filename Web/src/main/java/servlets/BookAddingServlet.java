package servlets;

import dto.BookDTO;
import entities.Author;
import services.AuthorService;
import services.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@WebServlet("/add")
public class BookAddingServlet extends HttpServlet {
    private BookService bookService = new BookService();
    private AuthorService authorService = new AuthorService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        String category = request.getParameter("category");
        String isbn = request.getParameter("isbn");
        String summary = request.getParameter("summary");
        String date = request.getParameter("date");
        Integer pages = Integer.parseInt(request.getParameter("pages"));
        Integer authorId = Integer.parseInt(request.getParameter("authorId"));
        Author author = authorService.findOne(authorId);

        LocalDate localDate = LocalDate.parse(date);
        BookDTO bookDTO = BookDTO.builder()
                .title(title)
                .category(category)
                .isbn(isbn)
                .release(localDate)
                .pages(pages)
                .borrow(false)
                .summary(summary)
                .author(author)
                .build();
        bookService.create(bookDTO);
        response.sendRedirect("/home");

    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Author> authors = authorService.findAllAuthors();
        request.setAttribute("authors", authors);
        request.getRequestDispatcher("addingForm.jsp").forward(request, response);
    }
}
