package servlets;

import exception.BookNotFoundException;
import services.BookService;
import services.BorrowerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "BorrowingServlet")
public class BorrowingServlet extends HttpServlet {

    private BookService bookService;
    private BorrowerService borrowerService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer bookId = Integer.valueOf(request.getParameter("bookId"));

        request.setAttribute("book", bookService.findOne(bookId));
        request.setAttribute("borrowers", borrowerService.findAllBorrowers());

        request.getRequestDispatcher("borrowBook.jsp").forward(request, response);
    }
}
