package servlets;

import entities.Borrow;
import entities.Borrower;
import entities.BorrowerDetails;
import enums.UserType;
import services.BorrowerDetailsService;
import services.BorrowerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register")
public class BorrowerRegisterServlet extends HttpServlet {
    private BorrowerService borrowerService = new BorrowerService();
    private BorrowerDetailsService borrowerDetailsService = new BorrowerDetailsService();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Borrower borrower = new Borrower();

        BorrowerDetails borrowerDetails = new BorrowerDetails();

        borrowerDetails.setEmail(request.getParameter("email"));
        borrowerDetails.setPhone(request.getParameter("phone"));
        String address = request.getParameter("street") + " " +  request.getParameter("housenumber") + " " + request.getParameter("postalcode") + " " + request.getParameter("city");
        borrowerDetails.setAddress(address);

        borrower.setFirstName(request.getParameter("firstname"));
        borrower.setLastName(request.getParameter("lastname"));
        borrower.setPassword(request.getParameter("username"));
        borrower.setUsername(request.getParameter("password"));
        borrower.setUserType(UserType.USER);
        borrowerDetailsService.saveBorrowerDetails(borrowerDetails);

        borrower.setBorrowerDetails(borrowerDetails);
        borrowerService.registerNewBorrower(borrower);
        borrowerDetails.setBorrower(borrower);

        request.getRequestDispatcher("/login.jsp").include(request,response);
    }

}
