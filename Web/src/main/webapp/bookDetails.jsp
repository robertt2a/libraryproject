<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Robert
  Date: 23.10.2019
  Time: 19:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<jsp:include page="WEB-INF/fragments/header.jspf"/>
<div class="container mt-4">
 <li >
     <i>${requestScope.book.title}</i>
     <i>${requestScope.book.authorFullName}</i>
     <i>${requestScope.book.release}</i>
     <i>${requestScope.book.category}</i>
     <i>${requestScope.book.isbn}</i>
     <i>${requestScope.book.pages}</i>
     <i>${requestScope.book.summary}</i>
 </li>
</div>

<a href="/home">
    <button type="button" class="btn btn-danger">< Back to book list</button>
</a>
<jsp:include page="WEB-INF/fragments/footer.jspf"/>
</body>
</html>
