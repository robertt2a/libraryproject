<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Homepage</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<jsp:include page="/WEB-INF/fragments/header.jspf"/>

<a href="/registerForm.jsp">
    <button type="button" class="btn btn-danger">Register</button>
</a>
<a href="/login.jsp">
    <button type="button" class="btn btn-danger">Log in</button>
</a>


<%@include file="/WEB-INF/fragments/footer.jspf" %>
</body>
</html>