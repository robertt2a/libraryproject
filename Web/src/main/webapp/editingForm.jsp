<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: rober_000
  Date: 2019-09-29
  Time: 10:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit book</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<form action="/edit" method="post" >
    <c:set var="book" value="${requestScope.book}"/>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label>Title</label>

            <input type="text" class="form-control" name="title" value="${book.getTitle()}">
            <input type="hidden" name="bookId" value="${book.getId()}">
        </div>
        <div class="form-group col-md-6">
            <label>Category</label>
            <input type="text" class="form-control" name="category" value="${book.getCategory()}">
        </div>
    </div>
    <div class="form-group col-md-6">
        <label>ISBN</label>
        <input type="number" class="form-control" name="isbn" placeholder="Enter ISBN number">
    </div>
    <div class="form-group col-md-3">
        <label>Book release date</label>
        <input type="date" class="form-control" name="date">
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label >Number of pages</label>
            <input type="number" class="form-control"name="pages" placeholder="Enter number of pages">
        </div>
    </div>
    <div class="form-group">
        <label >Author</label>
        <select class="form-control" name="authorId">
            <c:forEach var="author" items="${requestScope.authors}" >
                <option value="${author.getIdAuthor()}">${author.getDisplayName()}</option>
            </c:forEach>
        </select>
    </div>

    <div class="form-group col-md-6">
        <label>Summary</label>
        <input type="text" class="form-control" name="summary" placeholder="Enter book summary">
    </div>
    <button type="submit" class="btn btn-primary">Sign in</button>
</form>
</body>
</html>
