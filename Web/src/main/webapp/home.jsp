<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Library</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<jsp:include page="/WEB-INF/fragments/header.jspf"/>
<div class="container" @media (min-withd 1200px)>
    <form action="/home" method="post">
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Book Title</th>
                <th scope="col">Book Release Date</th>
                <th scope="col">ISBN Number</th>
                <th scope="col">Book Category</th>
                <th scope="col">Number Of Pages</th>
                <th scope="col">Borrower</th>
                <th scope="col">Author</th>
                <th scope="col">Action</th>

            </tr>
            </thead>
            <tbody>
            <c:forEach var="book" items="${requestScope.books}" varStatus="loop">
                <tr>
                    <th scope="row">${loop.index + 1}</th>
                    <td>${book.title}</td>
                    <td>${book.release}</td>
                    <td>${book.isbn}</td>
                    <td>${book.category}</td>
                    <td>${book.pages}</td>
                    <td>${book.borrowerName}</td>
                    <td>${book.authorFullName}</td>

                    <td><input class="form-check-input" type="radio" name="bookId" value="${book.id}"></td>

                </tr>
            </c:forEach>
            </tbody>
        </table>
        <input type="submit" class="btn btn-outline-success" name="action" value="ADD">
        <input type="submit" class="btn btn-outline-warning" name="action" value="EDIT">
        <input type="submit" class="btn btn-outline-primary" name="action" value="SHOW">
        <input type="submit" class="btn btn-outline-danger" name="action" value="DELETE">
    </form>
    <%@include file="/WEB-INF/fragments/footer.jspf" %>
</body>
</html>
