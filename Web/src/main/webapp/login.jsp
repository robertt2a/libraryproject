<%--
  Created by IntelliJ IDEA.
  User: Robert
  Date: 13.11.2019
  Time: 20:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<form action="/login" method="post">
    <div class="form-group">
        <div class="col-5">
            <label for="formGroupExampleInput">Username</label>
            <input type="text" class="form-control" name="Username" id="formGroupExampleInput" placeholder="Username">
        </div>
    </div>
    <div class="form-group">
        <div class="col-5">
            <label for="formGroupExampleInput2">Password</label>
            <input type="password" class="form-control" name="Password" id="formGroupExampleInput2" placeholder="Password">
        </div>
    </div>
    <input type="hidden" name="action" value="GET">
    <button type="submit" name="login" class="btn btn-primary">Log in</button>
    <button type="submit" name="adminLogin"  class="btn btn-primary">Log in as Administrator</button>
</form>
</body>
</html>
