package entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_id")
    private Integer idBook;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "pages", nullable = false)
    private Integer pages;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "category", nullable = false)
    private String category;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "isbn", nullable = false)
    private String isbn;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "summary", nullable = false)
    private String summary;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "title", nullable = false)
    private String title;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "borrow", nullable = false)
    private Boolean borrow;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "release_date", nullable = false)
    private LocalDate releaseDate;

    @OneToMany(mappedBy = "book")
    private List<Borrow> borrows;

    public Boolean isBorrowed(){
        return borrow;
    }

    public Book(Integer pages, String category, String isbn, String title, Boolean borrow, LocalDate releaseDate, String summary, Author author) {
        this.pages = pages;
        this.category = category;
        this.isbn = isbn;
        this.title = title;
        this.borrow = borrow;
        this.releaseDate = releaseDate;
        this.summary = summary;
        this.author = author;
    }
    public Book(Integer id, Integer pages, String category, String isbn, String title, Boolean borrow, LocalDate releaseDate, String summary, Author author) {
        this.idBook = id;
        this.pages = pages;
        this.category = category;
        this.isbn = isbn;
        this.title = title;
        this.borrow = borrow;
        this.releaseDate = releaseDate;
        this.summary = summary;
        this.author = author;
    }
}
