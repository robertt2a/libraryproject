package entities;

import enums.UserType;
import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "librarian")
public class Librarian {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "librarian_id")
    @Setter(AccessLevel.NONE)
    private Integer idLibrarian;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "username", nullable = false)
    private String username;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "password", nullable = false)
    private String password;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "email", nullable = false)
    private String email;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "user_type", nullable = false)
    private UserType userType;
}
