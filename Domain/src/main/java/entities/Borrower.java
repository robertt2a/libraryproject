package entities;

import enums.UserType;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "borrower")
public class Borrower {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "borrower_id")
    @Setter(AccessLevel.NONE)
    private Integer idBorrower;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "username")
    private String username;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "password")
    private String password;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "first_name")
    private String firstName;


    @Basic(fetch = FetchType.LAZY)
    @Column(name = "last_name")
    private String lastName;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "user_type", nullable = false)
    private UserType userType;

    @OneToOne
    @JoinColumn(name = "id_borrower_details")
    private BorrowerDetails borrowerDetails;

    @OneToMany(mappedBy = "borrower")
    private List<Borrow> borrows;

    public String getDisplayName() {
        return firstName + " " + lastName;
    }

}
