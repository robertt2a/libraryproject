package entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Setter
@Builder
@Getter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "author")
public class Author {
    @Id
    @Column(name = "author_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idAuthor;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "birth_place", nullable = false)
    private String birthPlace;

    @OneToMany(mappedBy = "author")
    private List<Book> books;

    public String getDisplayName() {
        return firstName + " " + lastName;
    }

    public Integer getIdAuthor() {
        return idAuthor;
    }
}
