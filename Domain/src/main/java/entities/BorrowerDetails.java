package entities;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Setter
@Getter
@Table(name = "borrower_details")
public class BorrowerDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "borrower_details_id")
    @Setter(AccessLevel.NONE)
    private Integer idBorrowerDetails;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "address")
    private String address;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "email", nullable = false)
    private String email;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "phone", nullable = false)
    private String phone;

    @OneToOne(mappedBy = "borrowerDetails")
    private Borrower borrower;
}
