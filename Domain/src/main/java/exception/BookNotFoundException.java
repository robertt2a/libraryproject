package exception;

public class BookNotFoundException extends Exception {
    public BookNotFoundException(String messagge){
        super(messagge);
    }
}
