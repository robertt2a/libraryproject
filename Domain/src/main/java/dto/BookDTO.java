package dto;

import entities.Author;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BookDTO {
    private Integer id;
    private String title;
    private LocalDate release;
    private String isbn;
    private String category;
    private Integer pages;
    private String borrowerName;
    private Boolean borrow;
    private String authorFullName;
    private String summary;
    private Author author;


}
