package enums;

public enum Action {
    ADD, EDIT, SHOW, DELETE, BORROW, BORROWED, GET, LOGOUT;
}
