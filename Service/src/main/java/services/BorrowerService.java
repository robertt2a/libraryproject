package services;

import entities.Borrower;
import repositories.BorrowerRepository;

import java.util.List;

public class BorrowerService {
    private BorrowerRepository borrowerRepository = new BorrowerRepository();

    public void registerNewBorrower(Borrower borrower){
        borrowerRepository.create(borrower);
    }

    public List<Borrower> findAllBorrowers(){
        return borrowerRepository.findAll();
    }
}
