package services;

import dto.BookDTO;
import entities.Author;
import entities.Book;
import entities.Borrow;
import entities.Borrower;
import repositories.BookRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BookDTOAssembler {

    private BookService bookService = new BookService();

    public List<BookDTO> mapAll() {
        BookRepository bookRepository = new BookRepository();
        List<Book> books = bookRepository.findAll();
        return books.stream()
                .map(b -> new BookDTO(b.getIdBook(),
                        b.getTitle(),
                        b.getReleaseDate(),
                        b.getIsbn(),
                        b.getCategory(),
                        b.getPages(),
                        checkIfBorrowerIsNull(b),
                        b.getBorrow(),
                        b.getAuthor().getDisplayName(),
                        b.getSummary(),
                        b.getAuthor()))
                .collect(Collectors.toList());
    }
    private String checkIfBorrowerIsNull(Book book){
        String borrowerName;
        if (bookService.findCurrentBorrower(book) == null){
            borrowerName = "-";
        } else {
            borrowerName = bookService.findCurrentBorrower(book);
        }
        return borrowerName;
    }
}
