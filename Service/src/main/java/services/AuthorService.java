package services;

import entities.Author;
import repositories.AuthorRepository;

import java.util.List;

public class AuthorService {
    private AuthorRepository authorRepository = new AuthorRepository();
    public List<Author> findAllAuthors(){
        return authorRepository.findAll();
    }
    public Author findOne(Integer id){
        return authorRepository.read(id);
    }
}
