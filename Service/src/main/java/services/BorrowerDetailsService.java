package services;

import entities.BorrowerDetails;
import repositories.BorrowerDetailsRepository;

public class BorrowerDetailsService {
    private BorrowerDetailsRepository borrowerDetailsRepository = new BorrowerDetailsRepository();

    public void saveBorrowerDetails(BorrowerDetails borrowerDetails){
        borrowerDetailsRepository.create(borrowerDetails);
    }
}
