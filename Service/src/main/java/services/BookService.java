package services;

import dto.BookDTO;
import entities.Book;
import entities.Borrow;
import entities.Borrower;
import repositories.BookRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;


public class BookService {
    private BookRepository bookRepository = new BookRepository();

    public List<BookDTO> findAll() {
        BookDTOAssembler assembler = new BookDTOAssembler();
        return assembler.mapAll();
    }
    public Book findOne(Integer id){
        return bookRepository.read(id);
    }

    public BookDTO findBook(Integer id){
        Book book = bookRepository.read(id);
        return new BookDTO(book.getIdBook(), book.getTitle(), book.getReleaseDate(), book.getIsbn(), book.getCategory(),book.getPages(), findCurrentBorrower(book), book.getBorrow(),book.getAuthor().getDisplayName(),book.getSummary(),book.getAuthor());
    }

    public void deleteBook(Integer id){
        bookRepository.delete(id);
    }

    public void create(BookDTO bookDTO) {
        Book book = Optional.of(bookDTO).map(dto -> new Book(dto.getPages(), dto.getCategory(), dto.getIsbn(), dto.getTitle(), dto.getBorrow(), dto.getRelease(), dto.getSummary(), dto.getAuthor())).get();
        bookRepository.create(book);
    }

    public void update(BookDTO bookDTO){
        Book book = Optional.of(bookDTO).map(dto -> new Book(dto.getId(), dto.getPages(), dto.getCategory(), dto.getIsbn(), dto.getTitle(), dto.getBorrow(), dto.getRelease(), dto.getSummary(), dto.getAuthor())).get();
        bookRepository.update(book);
    }

    public String findCurrentBorrower(Book book) {
        String borrowerName = null;
        if (book.isBorrowed()) {
            Optional<Borrower> borrower = book.getBorrows().stream()
                    .max(Comparator.comparing(Borrow::getIdBorrow))
                    .map(Borrow::getBorrower);
            if (borrower.isPresent()) {
                borrowerName = borrower.get().getDisplayName();
            }
        }
        return borrowerName;
    }
}
