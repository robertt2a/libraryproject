package services;

import entities.Librarian;
import repositories.LibrarianRepository;

import java.util.List;

public class LibrarianService {
    private LibrarianRepository librarianRepository = new LibrarianRepository();

    public void registerNewLibrarian(Librarian librarian) {
        librarianRepository.create(librarian);
    }

    public List<Librarian> findAllLibrarians() {
        return librarianRepository.findAll();
    }
}
